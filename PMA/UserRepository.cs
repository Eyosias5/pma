﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMA
{
    class  UserRepository
    {
       
        private IMongoCollection<User> _usersCollection;

        public UserRepository()
        {
            dbconnect db = new dbconnect();
            var _database = db.start();
            _usersCollection = _database.GetCollection<User>("users");
        }

        public async Task InsertUser(User user)
        {
            await _usersCollection.InsertOneAsync(user);
        }

        public async Task<List<User>> GetAllUsers()
        {
            return await _usersCollection.Find(new BsonDocument()).ToListAsync();
        }
        public async Task<List<User>> GetUsersByID(string id)
        {
            var filter = Builders<User>.Filter.Eq("_id",id);
            var result = await _usersCollection.Find(filter).ToListAsync();
            return result;
        }
   
    }
}
