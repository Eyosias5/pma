﻿
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PMA
{
    /// <summary>
    /// Interaction logic for Signup.xaml
    /// </summary>
    public partial class Signup : Window
    {
        public Signup()
        {
            InitializeComponent();
        }

        private void Button_Click_Login(object sender, RoutedEventArgs e)
        {
            MainWindow lp = new MainWindow();
            this.Visibility = Visibility.Hidden;
            lp.Show();
        }

        private async void Button_Click_Signup(object sender, RoutedEventArgs e)
        {

            User user = new User
            {
                Id = ID.Text.ToLower(),
                FirstName = FNAME.Text,
                LastName = LNAME.Text,
                Password = PASSWORD.Password,
            };

            UserRepository UR = new UserRepository();
            try
            {
                await UR.InsertUser(user);
            }
            catch (Exception ex) { Console.WriteLine("duplicate data founded"); };
            
           
            
        }

    }


}
