﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMA
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
   
    public partial class MainWindow : Window
    {
        public static string name;
        public MainWindow()
        {
            InitializeComponent();
            Console.WriteLine("Error occured");
        }

        private void Button_Click_SignUp(object sender, RoutedEventArgs e)
        {
            Signup sp = new Signup();
            this.Visibility = Visibility.Hidden;
            sp.Show();

        }

        private  async void Button_Click_Login(object sender, RoutedEventArgs e)
        {

            UserRepository UR = new UserRepository();


            try
            {
                List<User> List = await UR.GetUsersByID(ID.Text.ToLower());
                BsonDocument Bd = List[0].ToBsonDocument();

                name = Bd["FirstName"].AsString;
                name = name + " " + Bd["LastName"].AsString;

                if (string.Equals(Bd["Password"].AsString, PASSWORD.Password)) 
                {
                    Dashboard board = new Dashboard();
                    this.Visibility = Visibility.Hidden;
                    board.Show();
                }
            }
            catch (Exception er) {
                Console.WriteLine("THat kind of user is not found");
            }

       
         

          

        }
  

    }
}
