﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PMA
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Window
    {
        public Dashboard()
        {
            InitializeComponent();
            Main.Content = new DashboardPage();

        }


        private void MenuItem_Click_DashBoard(object sender, RoutedEventArgs e)
        {
            Main.Content = new DashboardPage();
        }
        private void MenuItem_Click_Profile(object sender, RoutedEventArgs e)
        {
            Main.Content = new ProfilePage();

        }
        private void MenuItem_Click_Despense(object sender, RoutedEventArgs e)
        {
           

        }
        private void MenuItem_Click_Inventory(object sender, RoutedEventArgs e)
        {
            Main.Content = new InventoryPage();

        }
        private void MenuItem_Click_Invoice(object sender, RoutedEventArgs e)
        {
            Main.Content = new InvoicePage();
        }
        private void MenuItem_Click_Logout(object sender, RoutedEventArgs e)
        {

        }
    }
}
