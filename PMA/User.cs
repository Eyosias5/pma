﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMA
{
    class User
    {  
        
        public String FirstName { get; set; }
       
        public String LastName { get; set; }
       
        public String Id { get; set; }
       
        public  String Password { get; set; }
    }
}
