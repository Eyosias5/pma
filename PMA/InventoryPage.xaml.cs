﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMA
{
    /// <summary>
    /// Interaction logic for InventoryPage.xaml
    /// </summary>
    public partial class InventoryPage : Page
    {
        public  InventoryPage()
        {
            InitializeComponent();
            Tables.Content = new ProductTablePage();

            Drug d = new Drug();
            DrugRepository dr = new DrugRepository();
            

            d.ItemName = "AGE-INTERRUPTER";
            d.Vendor = "Skinceuticals";
            d.Manufacturer = "Skinceuticals";
            d.PackageName = "Bottle";
            d.Stock = 30.03;
            d.Expiration = "05/31/2019";

         
            
           
           
            //InventoryData.Items.Add(d); 

        }

   
        private void ListBoxItem_Selected_Orders(object sender, RoutedEventArgs e)
        { 
            Tables.Content = new OrderTablePage();

        }
        private void ListBoxItem_Selected_Deposits(object sender, RoutedEventArgs e)
        {
            Tables.Content = new DepositeTablePage();
        }
        private void ListBoxItem_Selected_Alerts(object sender, RoutedEventArgs e)
        {
            Tables.Content = new AlertTablePage();
        }

        private void ListBoxItem_Selected_Products(object sender, RoutedEventArgs e)
        {
            try { Tables.Content = new ProductTablePage(); } catch (Exception ex) { return; }
           
        }

        private async void Button_Click_ADD(object sender, RoutedEventArgs e)
        {
            DateTime dateTime = DateTime.UtcNow.Date;
            var itemname = ITEMNAME.Text;
            var packagename = PACKAGENAME.Text;
            var vendor = VENDOR.Text;
            var expdate = EXPDATE.Text;
          

            Drug drug = new Drug
            {
                ItemName = ITEMNAME.Text.ToUpper(),
                PackageName = PACKAGENAME.Text.ToUpper(),
                Vendor = VENDOR.Text.ToUpper(),
                Manufacturer = MANUFACTURER.Text.ToUpper(),
                Expiration = EXPDATE.Text,
                OrderDate = dateTime.ToString("dd/MM/yyyy"),
                Stock = Convert.ToDouble(STOCK.Text),
                DepositeDate = null,
                OrderdBy = MainWindow.name,
            };

            DrugRepository DR = new DrugRepository();
            try
            {
                await DR.InsertDrug(drug);
            }
            catch (Exception ex) { Console.WriteLine("duplicate data founded"); };
            ITEMNAME.Text = String.Empty;
            PACKAGENAME.Text = String.Empty;
            MANUFACTURER.Text = String.Empty;
            EXPDATE.Text = String.Empty;
            STOCK.Text = String.Empty;
            VENDOR.Text = String.Empty;
            Tables.Content = null;
            Tables.Content = new ProductTablePage();
            popupbox.IsPopupOpen = false;
           

          
        }
        private  void Button_Click_Cancel(object sender, RoutedEventArgs e)
        {
            popupbox.IsPopupOpen = false;
        }




    }

}
