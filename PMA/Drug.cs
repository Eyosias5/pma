﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMA
{
    class Drug
    {
        public ObjectId _id { get; set; }
        public string ItemName { get; set; }
        public double Stock { get; set; }
        public string Manufacturer { get; set; }
        public string Vendor { get; set; }
        public string Expiration { get; set; }
        public string PackageName { get; set; }
        public string DepositedBy { get; set; }
        public string OrderdBy { get; set; }
        public string OrderDate { get; set; }
        public string DepositeDate { get; set; }
    }
}
