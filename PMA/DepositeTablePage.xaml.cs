﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMA
{
    /// <summary>
    /// Interaction logic for DepositeTablePage.xaml
    /// </summary>
    public partial class DepositeTablePage : Page
    {
        public DepositeTablePage()
        {
            InitializeComponent();
            FetchDrugs();
          
           
          


        }

        private async void FetchDrugs()
        {
            DrugRepository DR = new DrugRepository();
            try
            {
                List<Drug> List = await DR.GetAllDrugs();
                
                
             
                for(var i =0; i < List.Count; i++)
                {
                    BsonDocument Bd = List[i].ToBsonDocument();
                    var drugs = new ObservableCollection<Drug>();
                    Drug drug = new Drug {
                        ItemName = Bd["ItemName"].AsString,
                      // DepositedBy = Bd["DepositedBy"].AsString,
                        Stock = Bd["Stock"].AsDouble,
                        PackageName = Bd["PackageName"].AsString,
                        Vendor = Bd["Vendor"].AsString,
                      //  DepositeDate = Bd["DepositeDate"].AsString

                    };
                    
                    InventoryData.Items.Add(drug);

                    Console.WriteLine(Bd);
                }

                
            }catch(Exception exception)
            {
                Console.WriteLine("Exception" + exception);
            }
           
        }

     
    }
}
