﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMA
{
    class DrugRepository
    {
        public IMongoCollection<Drug> _drugsCollection;

        public DrugRepository()
        {
            dbconnect db = new dbconnect();
            var _database = db.start();
            _drugsCollection = _database.GetCollection<Drug>("drugs");
        }

        public async Task InsertDrug(Drug drug)
        {
            await _drugsCollection.InsertOneAsync(drug);
        }

        public async Task<List<Drug>> GetAllDrugs()
        {
            return await _drugsCollection.Find(new BsonDocument()).ToListAsync();
        }
        public async Task<List<Drug>> GetDrugsByID(string id)
        {
            var filter = Builders<Drug>.Filter.Eq("_id", id);
            var result = await _drugsCollection.Find(filter).ToListAsync();
            return result;
        }
        public async Task UpdateDrug(string _id,Drug d)
        {    Object ID = ObjectId.Parse(_id);
            
            var filter = Builders<Drug>.Filter.Eq("_id", ID );
            var update = Builders<Drug>.Update.Set("Stock",9999 );
            await _drugsCollection.UpdateOneAsync(filter, update);
        }
    }
}
