﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMA
{
    /// <summary>
    /// Interaction logic for ProductTablePage.xaml
    /// </summary>
    public partial class ProductTablePage : Page
    {
        public ProductTablePage()
        {
            InitializeComponent();
            FetchDrugs();
        }


        private async void FetchDrugs()
        {
            DrugRepository DR = new DrugRepository();
            try
            {
                List<Drug> List = await DR.GetAllDrugs();



                for (var i = 0; i < List.Count; i++)
                {
                    BsonDocument Bd = List[i].ToBsonDocument();
                   
                    Drug drug = new Drug
                    {
                        ItemName = Bd["ItemName"].AsString,
                        Stock = Bd["Stock"].AsDouble,
                        PackageName = Bd["PackageName"].AsString,
                        Manufacturer = Bd["Manufacturer"].AsString,
                        Vendor = Bd["Vendor"].AsString,
                        Expiration = Bd["Expiration"].AsString,
                        _id=Bd["_id"].AsObjectId,
                      

                    };

                    ProductData.Items.Add(drug);

                    Console.WriteLine(Bd);
                }


            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception" + exception);
            }

        }

      

        private void ProductData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String OItemName = ((dynamic)ProductData.SelectedItem).ItemName;
            Double OStock = ((dynamic)ProductData.SelectedItem).Stock;
            String OManufacturer = ((dynamic)ProductData.SelectedItem).Manufacturer;
            String OVendor = ((dynamic)ProductData.SelectedItem).Vendor;
            String OPackageName = ((dynamic)ProductData.SelectedItem).PackageName;
            String OExpiration = ((dynamic)ProductData.SelectedItem).Expiration;
            ObjectId O_id = ((dynamic)ProductData.SelectedItem)._id;
            Console.WriteLine(O_id.ToString());
            OverviewItemName.Text = OItemName;
            OverviewPackageName.Text = OPackageName;
            OverviewStock.Text = OStock.ToString();
            OverviewEpiration.Text = OExpiration;
            OverviewManufacturer.Text = OManufacturer;
            OverviewVendor.Text = OVendor;
            OverviewObjectId.Text = O_id.ToString();
   
            var row = ProductData.Items.IndexOf(ProductData.CurrentItem);
            Console.WriteLine(row);
        }

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            
            
                OverviewVendor.IsReadOnly = false;
                OverviewItemName.IsReadOnly = false;
                OverviewPackageName.IsReadOnly = false;
                OverviewStock.IsReadOnly = false;
                OverviewManufacturer.IsReadOnly = false;
                OverviewEpiration.IsReadOnly = false;

                Update.Visibility = System.Windows.Visibility.Visible;
                Delete.Visibility = System.Windows.Visibility.Visible;
            
      
      
        }
        private void ToggleButton_UnChecked(object sender, RoutedEventArgs e)
        {
                
            
                OverviewVendor.IsReadOnly = true;
                OverviewItemName.IsReadOnly = true;
                OverviewPackageName.IsReadOnly = true;
                OverviewStock.IsReadOnly = true;
                OverviewManufacturer.IsReadOnly = true;
                OverviewEpiration.IsReadOnly = true;

                Update.Visibility = System.Windows.Visibility.Hidden;
                Delete.Visibility = System.Windows.Visibility.Hidden;
            
        }

        private async void Update_Click(object sender, RoutedEventArgs e)
        {
            DrugRepository DR = new DrugRepository();
            Console.WriteLine(OverviewObjectId.Text);
            String id =OverviewObjectId.Text;
            String stock = OverviewStock.Text;
            Drug d = new Drug();



            try
            {
               await DR.UpdateDrug(id,stock);

            } catch (Exception ex){ Console.WriteLine(ex); }
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
